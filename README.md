# Servicio de eventos de la prueba técnica de SOSAFE

> Walter Devia

**[Consultar API GraphQL del servicio](https://events-service.walterdl.now.sh/api)**

## Advertencia de rendimiento

El despliegue se encuentra en un entorno gratuito y por tanto limitado. Pueden haber retrasos en las respuestas o peticiones http perdidas.

## Propósito

Provee mediante API graphql operaciones de consulta y creación de registros de eventos, además de notificaciones push de eventos creados.

## Ejecución local

Ejecutar `npm run start-dev` tras instalar dependencias. Notar que se requieren variables de entorno con claves de API [Pusher Channel](https://pusher.com/channels) y cadena de conexión MongoDB. Puede crear un archivo `.env` basado del archivo `.env.example` para iniciar.

## Tecnologías

- NodeJS
- TypeScript 3+
- [Apollo Server](https://www.apollographql.com/docs/apollo-server/)
- Pusher
- GraphQL
- [Mongoose](https://mongoosejs.com/)

## Estándar de codificación

El proyecto usa `ESLint` junto con las extensiones para TypeScript. Antes de cada commit el análisis de cumplimiento del estandar se ejecuta automáticamente para asegurar que no se incluya en el repositorio código que infrinja el estandar. Esto se hace mediante la librería [husky](https://www.npmjs.com/package/husky`). Ver las reglas del estandar en el archivo `.eslintrc.json`.

## Estructura del proyecto

Dado el comportamiento predeterminado de [Zeit Now para APIs NodeJS](https://zeit.co/docs/v2/serverless-functions/supported-languages/#node.js), se hace necesario ubicar los módulos que no proveen API http en la carpeta `_src`.

### Infraestructura

Las unidades de infraestructura del proyecto se encuentran en la carpeta `infrastructure`, las cuales son: logger, conector de mongo, emisor de notificaciones push de eventos creados y un servidor graphql. Algunas de estas unidades son usadas sólo al inicio del servicio pero otras son de uso compartido como el emisor de notificaciones y el logger.

### GQL

La carpeta `gql` contiene toda la definición de la API graphql que expone el servicio, esto es la definición del esquema graphql y los resolutores correspondientes. Los resolutores pueden entenderse como controladores http en el patrón MVC los cuales responden ante una solicitud http POST a un determinado campo del esquema graphql.

Puede encontrar una documentación autogenerada de la API graphql visitando la url del servicio en un navegador web.

### Modelos y repositorios

El servicio solo administra un modelo de negocio: el evento. Este modelo y el repositorio que permite su gestión se encuentran en las carpetas `event-model` y `event-repository` respectivamente.

## Patrones

### Repository

Para favorecer un bajo acoplamiento con la tecnología de persistencia se hace uso del [patrón de diseño repository](https://medium.com/@pererikbergman/repository-design-pattern-e28c0f3e4a30). El repositorio `event-repository` expone una interfaz que entrega objetos primitivos de tipo `EventModel`, pero la definición de sus métodos utiliza la API de mongoose para consultar/crear registros.

## Despliegue

Desplegado en [Zeit Now](https://zeit.co/) como [servicio serverless](https://zeit.co/docs/v2/serverless-functions/introduction/) con despliegue contínuo habilitado integrado con GitLab. Los últimos cambios se ven reflejados en la url del servicio tras fusionar commits a la rama master del repositorio.
