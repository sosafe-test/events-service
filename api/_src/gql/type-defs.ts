
import { gql } from 'apollo-server-micro'

export default gql`
  type Mutation {
    createEvent(name: String!, coordinate: CoordinateInput!, description: String): Event
  }

  type Query {
    events(keywords: [String]!): [Event]!
  }

  type Event {
    id: ID!
    name: String!
    coordinate: Coordinate!
    description: String
  }

  type Coordinate {
    lat: Float
    lng: Float
  }

  input CoordinateInput {
    lat: Float
    lng: Float
  }
`