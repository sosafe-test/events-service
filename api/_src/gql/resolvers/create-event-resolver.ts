// Own
import { Resolver } from './types'
import EventRepository from '../../event-repository'
import { Coordinate } from '../../event-model/types'
import notifyEventCreated from '../../infrastructure/utils/event-creation-notifier'

interface Args {
  name: string;
  description: string;
  coordinate: Coordinate;
}

export default class CreateEventResolver implements Resolver {
  private eventRepository: EventRepository

  constructor() {
    this.eventRepository = new EventRepository()
  }

  public async execute(_: any, args: Args): Promise<any> {
    const event = await this.eventRepository.createEvent(args)
    notifyEventCreated(event)
    return event
  }

}
