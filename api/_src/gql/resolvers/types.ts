export interface ResolverExecutor {
  (root: any, args: any, ctx: any): Promise<any>
}

export interface Resolver {
  execute: ResolverExecutor
}

export interface DefaultResolvers {
  Mutation?: {
    [key: string]: ResolverExecutor
  };
  Query?: {
    [key: string]: ResolverExecutor
  };
}

export type RootResolver = DefaultResolvers & {
  [key: string]: {
    [key: string]: ResolverExecutor
  }
};
