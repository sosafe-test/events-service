import { Resolver } from './types'
import EventRepository from '../../event-repository'

interface Args {
  keywords: string[];
}

export default class EventsQueryResolver implements Resolver {
  private eventRepository: EventRepository

  constructor() {
    this.eventRepository = new EventRepository()
  }

  public async execute(_: any, args: Args): Promise<any> {
    return this.eventRepository.getEvents(args.keywords)
  }

}
