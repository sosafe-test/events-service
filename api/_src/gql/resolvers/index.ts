import CreateEventResolver from './create-event-resolver'
import EventsQueryResolver from './events-query-resolver'
import { RootResolver } from './types'

const rootResolver: RootResolver = {
  Mutation: {
    createEvent: (_, args) => new CreateEventResolver().execute(_, args)
  },
  Query: {
    events: (_, args) => new EventsQueryResolver().execute(_, args)
  }
}

export default rootResolver
