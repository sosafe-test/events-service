import { EventModel, Coordinate } from '../event-model/types'

export interface EventsRepository {
  createEvent: (data: { name: string; coordinate: Coordinate; description?: string; }) => Promise<EventModel>
  getEvents: (keywords: string[]) => Promise<EventModel[]>;
}
