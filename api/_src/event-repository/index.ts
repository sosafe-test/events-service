import { EventsRepository } from './types'
import { EventModel, Coordinate } from '../event-model/types'
import EventMongooseModel from '../event-model'
import logger from '../infrastructure/utils/logger'

export default class MongooseEventsRepository implements EventsRepository {
  public async createEvent(
    data: { name: string; coordinate: Coordinate; description?: string | undefined; }
  ): Promise<EventModel> {
    return EventMongooseModel.create(data)
      .then(newEvent => {
        newEvent = newEvent.toObject()
        newEvent.id = newEvent._id
        return newEvent
      })
      .catch(error => {
        logger().error(`There was an error creating event. Details: ${error}`)
        return Promise.reject('Internal server error creating event.')
      })
  }

  public async getEvents(keywords: string[]): Promise<EventModel[]> {
    const filter: any = {}

    if (keywords.length) {
      filter.name = { $in: keywords.map(x => new RegExp(x, 'i')) }
    }

    return this.fetchEvents(filter)
  }

  private async fetchEvents(filter: any): Promise<EventModel[]> {
    return EventMongooseModel.find(filter)
      .catch(error => {
        logger().error(`There was an error getting events with filter. Details: ${error}`)
        return Promise.reject('Internal server error getting events.')
      })
  }

}