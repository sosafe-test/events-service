export interface EventModel {
  id: string;
  name: string
  description?: string
  coordinate: Coordinate;
}

export interface Coordinate {
  lat: number;
  lng: number;
}