import { Document, Schema, model } from 'mongoose'

// Own
import { EventModel } from './types'

const eventSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: false
    },
    coordinate: {
      type: {
        lat: Number,
        lng: Number
      },
      required: true
    }
  }
)

export type EventDocument = EventModel & Document;

export default model<EventDocument>('Events', eventSchema, 'events')
