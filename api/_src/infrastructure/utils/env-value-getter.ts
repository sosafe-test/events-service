import logger from './logger'

/**
 * Given a key it returns the value from the environment values.
 * The key must follow the convention of nesting as dot separated. Eg:
 * @param configValueName The config value name to get from environment values.
 * @param muteLog Whether log an error if the environment value wasn't found. Default to false.
 * @returns String if the value was found otherwise undefined.
 */
export default function getConfigValue(configValueName: string, muteLog = false): string | undefined {
  if (
    typeof process.env[configValueName] !== 'string' ||
    !(process.env[configValueName] as string).trim().length
  ) {
    if (!muteLog) {
      logger().error(
        `Tried to get environment variable ${configValueName} but wasn't found.`
      )
    }

    return undefined
  }

  return process.env[configValueName]
}
