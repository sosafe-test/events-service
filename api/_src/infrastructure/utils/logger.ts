import { IncomingHttpHeaders } from 'http'
import { Logger as WinstonLogger, createLogger, format, config, transports } from 'winston'
import { NowRequest } from '@now/node'

// Own
// Constants
import {
  ENVIRONMENTS,
  getCurrentEnvironment
} from '../constants/environments'

let _logger: WinstonLogger

export default function getLogger(): WinstonLogger {
  if (!_logger) {
    _logger = createLogger({
      levels: config.npm.levels,
      format: format.json(),
      transports: [
        new transports.Console({
          level:
            getCurrentEnvironment() === ENVIRONMENTS.Prod ? 'info' : 'silly',
          format: format.simple()
        })
      ]
    })


    if (getCurrentEnvironment() !== ENVIRONMENTS.Prod) {
      _logger.debug('Logging initialized at debug level')
    }
  }

  return _logger
}

/**
 * Knows how to log in a pretty format the incoming request.
 */
export const logIncomingRequest = (req: NowRequest) => {
  let message = 'Request received with:\r\n'
  message += `  url = ${req.url}\r\n`
  message += `  method = ${req.method}\r\n`
  message += `  from ${req.headers['x-forwarded-for']}\r\n`
  message += `  date = ${getNow()}\r\n`
  message += `  headers:\r\n`

  const headers = incomingHTTPHeadersToObj(req.headers)

  for (const headerName in headers) {
    if (headers.hasOwnProperty(headerName)) {
      message += `    ${headerName}: ${headers[headerName]}`
      message += '\r\n'
    }
  }

  getLogger().info(message)
}

/**
 * Given an IncomingHttpHeaders instance it returns the headers as:
 * {
 *  <header name> = <header value>,
 *  <header name> = <header value>,
 *  ...
 * }
 */
function incomingHTTPHeadersToObj(incomingHTTPHeaders: IncomingHttpHeaders): { [index: string]: string } {
  const headers = {} as any

  for (const headerName in incomingHTTPHeaders) {
    if (incomingHTTPHeaders.hasOwnProperty(headerName)) {
      headers[headerName] = incomingHTTPHeaders[headerName]
    }
  }

  return headers
}

/**
 * Gives the current date and time. E.g "10/17/2018 at 11:31:16 AM"
 */
function getNow(): string {
  const currentDate = new Date()
  return `${currentDate.toLocaleDateString(
    'en-US'
  )} at ${currentDate.toLocaleTimeString('en-US')}`
}
