import mongoose from 'mongoose'

// Own
import { CONNECTION_STRING } from '../constants/mongo'
import logger from './logger'

let connected: boolean

/**
 * Performs the connection to Mongo.
 */
export default async function (): Promise<void> {
  if (connected) {
    return
  }

  return mongoose.connect(CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
      logger().info(`Mongo DB connected.`)
      connected = true
    })
    .catch(error => {
      logger().error(`Couldn't connect DB. Details: ${error}`)
      return Promise.reject('Internal server error starting repository.')
    })
}
