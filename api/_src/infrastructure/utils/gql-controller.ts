import { NowRequest, NowResponse } from '@now/node'
import { ApolloServer, gql } from 'apollo-server-micro'
import { DocumentNode } from 'graphql'

export default function (
  typeDefs: DocumentNode | string,
  resolvers: { [key: string]: any },
  path: string
): (req: NowRequest, res: NowResponse) => any {
  const server = new ApolloServer({
    typeDefs: typeof typeDefs === 'string' ? gql(typeDefs) : typeDefs,
    resolvers,
    introspection: true,
    playground: true
  })

  return server.createHandler({ path })
}
