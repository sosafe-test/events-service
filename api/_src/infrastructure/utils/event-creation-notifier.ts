import Pusher from 'pusher'
import { Response } from 'request'

// Own
import { EventModel } from '../../event-model/types'
import { PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET } from '../constants/pusher'
import logger from './logger'

const pusher = new Pusher({
  appId: PUSHER_APP_ID,
  key: PUSHER_KEY,
  secret: PUSHER_SECRET,
  cluster: 'us2',
  encrypted: true
})

export default function (event: EventModel) {
  pusher.trigger('web-clients', 'event-created', { event }, (error, _, res) => {
    if (error) {
      logger().error(`There was an error sending notification. Details: ${error}`)
      return
    }

    logNotificationResponse(res, event)
  })

  function logNotificationResponse(res: Response, event: EventModel) {
    logger().info(`Showing notification response...`)
    const resParsed = res.toJSON()

    try {
      logger().info(`Response raw body: ${res.body}`)
      logger().info(`Response body: ${resParsed.body}`)
      logger().info(`Response statusCode: ${resParsed.statusCode}`)
      logger().info(`EventModel: ${JSON.stringify(event)}`)
    } catch { }
  }
}
