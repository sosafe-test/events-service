import { NowResponse, NowRequest } from '@now/node'

export function setCors(res: NowResponse) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', '*')
  res.setHeader('Access-Control-Allow-Headers', '*')
}

export function isRequestForOptions(req: NowRequest): boolean {
  return typeof req.method === 'string' && req.method.toLowerCase() === 'options'
}