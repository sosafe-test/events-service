import envValueGetter from '../utils/env-value-getter'

const mongoConnectionString = envValueGetter('DB_CONNECTION_STRING')

if (!mongoConnectionString) {
  throw new Error(
    `Cannot initialize mongo constants due lack of environment values.
    None mongo connection string was found in environment values.`
  )
}

export const CONNECTION_STRING = mongoConnectionString
