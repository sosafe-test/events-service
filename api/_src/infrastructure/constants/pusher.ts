import envValueGetter from '../utils/env-value-getter'

const pusherAppId = envValueGetter('PUSHER_APP_ID')
const pusherKey = envValueGetter('PUSHER_KEY')
const pusherSecret = envValueGetter('PUSHER_SECRET')

if (!pusherAppId || !pusherKey || !pusherSecret) {
  throw new Error(
    `Cannot initialize pusher constants due lack of environment values.`
  )
}

export const PUSHER_APP_ID = pusherAppId
export const PUSHER_KEY = pusherKey
export const PUSHER_SECRET = pusherSecret
