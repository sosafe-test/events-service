import { NowRequest, NowResponse } from '@now/node'

// Own
import { setCors, isRequestForOptions } from './_src/infrastructure/cors'
import connectDB from './_src/infrastructure/utils/mongo-connector'
import gqlController from './_src/infrastructure/utils/gql-controller'
import gqlTypeDefs from './_src/gql/type-defs'
import gqlResolvers from './_src/gql/resolvers'

export default async (req: NowRequest, res: NowResponse) => {
  setCors(res)

  if (isRequestForOptions(req)) {
    res.writeHead(200)
    return res.end()
  }

  await connectDB()
  return gqlController(gqlTypeDefs, gqlResolvers, '/api')(req, res)
}
